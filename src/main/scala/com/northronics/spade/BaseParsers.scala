package com.northronics.spade

import java.lang.StringBuilder

import scala.collection.mutable

object BaseParsers {
	private val asciiZero = 48
	private val asciiNine = 57

	def char(character: Int): Parser[Int, Int] = matchAt(character)

	val noSignInteger: Parser[Int, String] =
		for {
			matched <- satisfy((a: Int) => a >= asciiZero && a <= asciiNine) +
		} yield buildString(matched: _*)

	val integer: Parser[Int, String] =
		for {
			sign <- (char('+') | char('-')).optional
			noSignInteger <- noSignInteger
		} yield sign.map(a => buildString(a)).getOrElse("") + noSignInteger

	val decimal: Parser[Int, String] =
		for {
			first <- integer
			_ <- char('.')
			second <- noSignInteger
			exponent <- (for {
				e <- char('e') | char('E')
				n <- integer
			} yield buildString(e) + n) ?
		} yield first + "." + second + exponent.getOrElse("")

	def matchAt[A](toMatch: A): Parser[A, A] =
		for {
			char <- satisfy[A](a => a == toMatch)
		} yield char

	def matchEmpty[A]: Parser[A, Unit] = Parser[A, Unit]((l, i) => Success(i, ()))

	def many[I, O](rule: Parser[I, O]): Parser[I, List[O]] =
		Parser[I, List[O]]((source, index) => {
			val buffer = mutable.Buffer[O]()
			var running = true
			var currentIndex = index
			while (currentIndex < source.length && running) {
				val result = rule(source, currentIndex)
				result match {
					case Success(nextIndex, returnValue) =>
						buffer += returnValue
						currentIndex = nextIndex
					case Failure =>
						running = false
				}
			}
			val matches = buffer.toList
			if (matches.isEmpty) {
				Failure
			} else {
				Success(currentIndex, matches)
			}
		})

	def notFollowedBy[I, O, P](first: Parser[I, O], second: Parser[I, P]): Parser[I, O] =
		Parser[I, O]((source, index) => {
			val firstResult = first(source, index)
			firstResult match {
				case Success(newIndex, value) =>
					val secondResult = second(source, newIndex)
					secondResult match {
						case Success(_, _) => Failure
						case Failure => Success(newIndex, value)
					}
				case Failure => Failure
			}
		})

	def optional[I, O](rule: Parser[I, O]): Parser[I, Option[O]] =
		Parser[I, Option[O]]((source, index) => {
			val result = rule(source, index)
			result match {
				case Success(newIndex, value) => Success(newIndex, Some(value))
				case Failure => Success(index, None)
			}
		})

	def or[I, O](first: Parser[I, O], second: Parser[I, O]): Parser[I, O] =
		Parser[I, O]((source, index) => {
			val firstResult = first(source, index)
			firstResult match {
				case Success(_, _) => firstResult
				case Failure => second(source, index)
			}
		})

	def satisfy[A](predicate: A => Boolean): Parser[A, A] =
		Parser[A, A]((l, i) => {
			if (i < l.length) {
				val current = l(i)
				if (predicate(current)) {
					Success(i + 1, current)
				} else {
					Failure
				}
			} else Failure
		})

	def string(text: String): Parser[Int, String] =
		Parser[Int, String]((source, index) => {
			var sourceIndex = index
			var ruleIndex = 0
			val ruleList = text.codePointList.map(a => char(a))
			val builder = new StringBuilder()
			var running = true
			var error = false
			while (ruleIndex < ruleList.length && sourceIndex < source.length && running) {
				val currentRule = ruleList(ruleIndex).apply(source, sourceIndex)
				currentRule match {
					case Success(nextIndex, value) =>
						sourceIndex = nextIndex
						builder.appendCodePoint(value)
						ruleIndex += 1
					case Failure =>
						error = true
						running = false
				}
			}
			if (error) {
				Failure
			} else {
				Success(sourceIndex, builder.toString)
			}
		})

	private def buildString(list: Int*): String = {
		var builder = new StringBuilder()
		for (index <- list.indices) {
			builder = builder.appendCodePoint(list(index))
		}
		builder.toString
	}
}
