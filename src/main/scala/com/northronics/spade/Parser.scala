package com.northronics.spade

case class Parser[I, +O](recognizer: (List[I], Int) => Result[O]) {
	def apply(source: List[I], index: Int = 0): Result[O] = recognizer(source, index)

	def flatMap[A](rule: O => Parser[I, A]): Parser[I, A] =
		Parser((source: List[I], index: Int) => {
			val result = recognizer(source, index)
			result match {
				case Success(nextIndex, returnValue) =>
					rule(returnValue)(source, nextIndex)
				case _ => Failure
			}
		})

	def ? : Parser[I, Option[O]] = optional

	def + : Parser[I, List[O]] = many

	def |[A >: O](rule: Parser[I, A]): Parser[I, A] = or(rule)

	def !>[A](rule: Parser[I, A]): Parser[I, O] = notFollowedBy(rule)

	def many: Parser[I, List[O]] = BaseParsers.many(this)

	def notFollowedBy[A](rule: Parser[I, A]): Parser[I, O] = BaseParsers.notFollowedBy(this, rule)

	def optional: Parser[I, Option[O]] = BaseParsers.optional(this)

	def or[A >: O](alternative: Parser[I, A]): Parser[I, A] =
		BaseParsers.or(this.asInstanceOf[Parser[I, A]], alternative)

	def map[A](f: O => A): Parser[I, A] =
		Parser((source: List[I], index: Int) => recognizer(source, index).map(a => f(a)))
}

object Parser {
	def runWithSource[A](source: String, rule: Parser[Int, A]): Result[A] =
		rule(source.codePointList, 0)
}

