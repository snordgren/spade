package com.northronics.spade

sealed abstract class Result[+A] {
	def map[B](f: A => B): Result[B]
}

case class Success[A](index: Int, value: A) extends Result[A] {
	def map[B](f: A => B) = Success(index, f(value))
}

case object Failure extends Result {
	def map[B](f: Nothing => B) = Failure
}
