package com.northronics

import scala.collection.mutable

package object spade {
	implicit class CodePointCapableString(val str: String) extends AnyVal {
		def codePointList: List[Int] = {
			val buffer = mutable.Buffer[Int]()
			var index = 0
			while (index < str.length) {
				val currentChar = str.codePointAt(index)
				buffer += currentChar
				index += Character.charCount(currentChar)
			}
			buffer.toList
		}
	}
}

