package com.northronics.spade

import com.northronics.spade.BaseParsers._
import org.scalatest.{FlatSpec, Matchers}

class ParserSpec extends FlatSpec with Matchers {

	private def testRuleWithSource[O](rule: Parser[Int, O], source: String, expectedIndex: Int, expectedValue: O) = {
		val result = Parser.runWithSource(source, rule)
		result match {
			case Success(returnIndex, returnValue) =>
				returnIndex should be(expectedIndex)
				returnValue should be(expectedValue)
			case _ => fail()
		}
	}

	private def testMatchesSource(rule: Parser[Int, String], source: String) =
		testRuleWithSource(rule, source, source.length, source)

	"A Parser" should "match an empty rule" in {
		val emptyRule = for {
			first <- matchEmpty[Int]
		} yield true
		val result = Parser.runWithSource("", emptyRule)

		result match {
			case Success(index, returnValue) =>
				index should be(0)
				returnValue should be(true)
			case Failure => fail()
		}
	}

	it should "match a character" in {
		val rule = for {
			matched <- char('.')
		} yield matched
		val result = Parser.runWithSource(".", rule)
		testRuleWithSource(rule, ".", 1, '.'.toInt)
	}

	it should "match two characters in a row" in {
		val rule: Parser[Int, String] = for {
			first <- char('a')
			second <- char('b')
		} yield "" + first.toChar + second.toChar
		testRuleWithSource(rule, "ab", 2, "ab")
	}

	it should "match any number of characters" in {
		val rule = for {
			matches <- char('a') +
		} yield matches
		for (i <- 1 to 128) {
			val source: String = (0 to i).map(a => "a").reduce((a, b) => a + b)
			val result = Parser.runWithSource(source, rule)
			result match {
				case Success(nextIndex, returnValue) =>
					nextIndex should be(i + 1)
					returnValue.length should be(source.length)
				case _ => fail()
			}
		}
	}

	it should "handle one match or another" in {
		val rule = for {
			matched <- char('a') | char('b')
		} yield matched
		testRuleWithSource(rule, "a", 1, 'a'.toInt)
		testRuleWithSource(rule, "b", 1, 'b'.toInt)
	}

	it should "handle an optional match" in {
		val rule = for {
			matched <- optional(char('a'))
		} yield matched
		testRuleWithSource(rule, "a", 1, Some('a'.toInt))
		testRuleWithSource(rule, "b", 0, None)
	}

	it should "handle nested matches" in {
		val ruleAB = for {
			matched <- char('a') | char('b')
		} yield matched
		val ruleABC = for {
			matched <- ruleAB | char('c')
		} yield matched
		testRuleWithSource(ruleABC, "a", 1, 'a'.toInt)
		testRuleWithSource(ruleABC, "b", 1, 'b'.toInt)
		testRuleWithSource(ruleABC, "c", 1, 'c'.toInt)
	}

	it should "match a keyword" in {
		val keywordValue = "text"
		val rule = string(keywordValue)
		testRuleWithSource(rule, keywordValue, keywordValue.length, keywordValue)
	}

	it should "handle an integer" in {
		for (value <- -128 to 128) {
			val source = value.toString
			testMatchesSource(integer, source)
		}
	}

	it should "handle a decimal" in {
		for (_ <- 0 to 128) {
			testMatchesSource(decimal, Math.random().toString)
		}
		testMatchesSource(decimal, "+1.2e-9")
		testMatchesSource(decimal, "-1.2e+9")
	}

	it should "handle a rule that can't be followed by another" in {
		val rule = integer.notFollowedBy(char('a'))
		val matchSource = "123"
		val noMatchSource = "123a"
		testMatchesSource(rule, matchSource)
		val noMatchResult = Parser.runWithSource(noMatchSource, rule)
		noMatchResult match {
			case Success(_, _) => fail()
			case Failure => ()
		}
	}

	it should "handle or operations with other rules with output types with shared super types" in {
		sealed abstract class Value
		case class DecimalValue(value: Float) extends Value
		case class IntegerValue(value: Int) extends Value

		val decimalParser = decimal.map(a => DecimalValue(a.toFloat))
		val integerParser = integer.map(a => IntegerValue(a.toInt))
		val parser: Parser[Int, Value] = decimalParser | integerParser

		val decimalResult = Parser.runWithSource("1.0", parser)
		decimalResult match {
			case Success(_, returnVal) =>
				returnVal should be(DecimalValue(1.0f))
			case Failure => fail()
		}

		val integerResult = Parser.runWithSource("1", parser)
		integerResult match {
			case Success(_, returnVal) =>
				returnVal should be(IntegerValue(1))
			case Failure => fail()
		}
	}
}
