package com.northronics.spade

import com.northronics.spade.BaseParsers._
import org.scalatest.{FlatSpec, Matchers}

class CalculatorGrammarTest extends FlatSpec with Matchers {
	val bound = 128

	val addition: Parser[Int, Int] =
		for {
			first <- integer
			_ <- char('+')
			second <- integer
		} yield first.toInt + second.toInt

	val subtraction: Parser[Int, Int] =
		for {
			first <- integer
			_ <- char('-')
			second <- integer
		} yield first.toInt - second.toInt

	"A calculation parser" should "parse addition" in {
		for (a <- 0 to bound) {
			for (b <- 0 to bound) {
				val source = a.toString + "+" + b.toString
				val result = addition(source.codePointList)
				result match {
					case Success(nextIndex, returnValue) =>
						nextIndex should be(source.length)
						returnValue should be(a + b)
					case Failure => fail()
				}
			}
		}
	}

	it should "parse subtraction" in {
		for (a <- 0 to bound) {
			for (b <- 0 to bound) {
				val source = a.toString + "-" + b.toString
				val result = subtraction(source.codePointList)
				result match {
					case Success(nextIndex, returnValue) =>
						nextIndex should be(source.length)
						returnValue should be(a - b)
					case Failure => fail()
				}
			}
		}
	}


	it should "parse addition or subtraction" in {
		val rule = addition | subtraction
		for (a <- 0 to bound) {
			for (b <- 0 to bound) {
				val additionSource = a.toString + "+" + b.toString
				val additionResult = rule(additionSource.codePointList)
				additionResult match {
					case Success(index, returnVal) =>
						index should be(additionSource.length)
						returnVal should be(a + b)
					case Failure => fail()
				}

				val subtractionSource = a.toString + "-" + b.toString
				val subtractionResult = rule(subtractionSource.codePointList)
				subtractionResult match {
					case Success(index, returnVal) =>
						index should be(subtractionSource.length)
						returnVal should be(a - b)
					case Failure => fail()
				}
			}
		}
	}
}
